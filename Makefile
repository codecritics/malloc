CC=gcc
CFLAGS= -fPIC -std=c99 -Wall -Werror -Wextra -pedantic
SRC= src/malloc.c src/block1.c src/page.c
all: libmalloc

check: all main
	LD_PRELOAD=./libmalloc.so ./tests/tests

tree: all 
	LD_PRELOAD=./libmalloc.so tree

project:
	LD_PRELOAD=./tests/libmalloc.so make all

level7: all
	LD_PRELOAD=./libmalloc.so ./tests/level7

level11: all 
	LD_PRELOAD=./libmalloc.so ./tests/level11

level9: all 
	LD_PRELOAD=./libmalloc.so ./tests/level9

level10: all
	LD_PRELOAD=./libmalloc.so python ./tests/level10.py

ls: all 
	LD_PRELOAD=./libmalloc.so ls

main:
	$(CC) $(CFLAGS) tests/$@.c -o tests/tests 

libmalloc: $(SRC)
	$(CC) $(CFLAGS) -shared -o $@.so $^

distclean: 
	rm -f *.o
	rm -f tests/tests

clean: distclean
	rm -f *.so *.a
