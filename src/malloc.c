#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include "page.h"
#include "block1.h"
static void merge_block(struct block *tmp_block)
{
    while (tmp_block->next && tmp_block->next->free == 1)
    {
        tmp_block->size = tmp_block->size + sizeof (struct block)
        + tmp_block->next->size;
        if (tmp_block->next->next != NULL)
            tmp_block->next->next->previous = tmp_block;
        tmp_block->next = tmp_block->next->next;
    }
}

static void free_page(struct page *p)
{
    if (p->next == NULL && p->previous == NULL)
    {
        munmap(p, sysconf(_SC_PAGESIZE) * p->ratio_pages);
        use_page(NULL);
    }
    else
    {
        struct page *previous = p->previous;
        struct page *next = p->next;
        if (previous != NULL)
        {
            previous->next = next;
            use_page(previous);
        }
        if (next)
        {
            next->previous = previous;
            use_page(next);
        }
        munmap(p, sysconf(_SC_PAGESIZE) * p->ratio_pages);
    }
}
void *malloc(size_t size)
{
    if (size  == 0)
        return NULL;
    if (check_page() == NULL)
    {
        struct page *np = create_page(size);
        init_page(np, size);
        use_page(np);
        init_block(np->start, np->nb_alloc - sizeof(struct block));
    }
    void *data = new_block(size);
    return data;
}

void free(void *ptr)
{
    if (ptr == NULL)
    {
        return;
    }
    struct block *tmp_block;
    tmp_block = ptr;
    --tmp_block;
    if (tmp_block->data != ptr || tmp_block->free == 1)
        return;
    tmp_block->free = 1;
    struct page *np = tmp_block->page;
    np->nb_alloc = np->nb_alloc + tmp_block->size + sizeof (struct block);
    merge_block(tmp_block);
    if (np->nb_alloc >= ((np->ratio_pages) * sysconf(_SC_PAGESIZE))
            - sizeof (struct page)
            - sizeof (struct block))
    {
        free_page(np);
    }
}

void *calloc(size_t number, size_t size)
{
    if (size == 0 || number == 0)
        return NULL;
    void *ptr = malloc(number * size);
    if (ptr == NULL)
        return NULL;
    return memset(ptr, 0, number * size);
}
void *realloc(void *ptr, size_t size)
{
    if (!ptr)
    {
        return malloc(size);
    }
    struct block *tmp_block;
    tmp_block = ptr;
    tmp_block = tmp_block - 1;
    if (tmp_block->size >= size)
    {
        return ptr;
    }
    void *tmp_ptr = malloc(size);
    if (tmp_ptr == NULL)
    {
        return NULL;
    }
    memcpy(tmp_ptr, ptr, tmp_block->size);
    free(ptr);
    return tmp_ptr;
}
