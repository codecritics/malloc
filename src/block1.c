#include <unistd.h>
#include <stdlib.h>
#include "block1.h"
#include "page.h"
// Initilazation of the block
struct block *research_block(size_t size)
{
    struct page *np = check_page();
    struct page *prev;
    while (np != NULL)
    {
        struct block *tmp_block = np->start;
        while (tmp_block != NULL)
        {
            if (tmp_block->free == 1 &&
                    tmp_block->size > size + sizeof(struct block))
            {
                use_page(np);
                return tmp_block;
            }
            tmp_block = tmp_block->next;
        }
        prev = np;
        np = np->next;
    }
    use_page(prev);
    return NULL;
}

void add_page(size_t size)
{
    struct page *np = create_page(size);
    init_page(np, size);
    return_page();
    struct page *p = check_page();
    while (p->next != NULL)
    {
        p = p->next;
    }
    p->next = np;
    np->previous = p;
    use_page(np);
    init_block(np->start, np->nb_alloc - sizeof (struct block));
}

void init_block(struct block *block, size_t size)
{
    block->size =  size;
    block->next = NULL;
    block->previous = NULL;
    block->data = block + 1;
    block->free = 1;
    block->page = check_page();
}
void split_block(struct block *tmp_block, size_t size)
{
    void *v = tmp_block;
    char *c = v;
    c = c + sizeof (struct block) + size;
    v = c;
    struct block *tmp_block1 = v;
    tmp_block1->next = tmp_block->next;
    tmp_block1->free = 1;
    tmp_block1->size = tmp_block->size - size - sizeof (struct block);
    tmp_block1->previous = tmp_block;
    tmp_block1->data = tmp_block1 + 1;
    tmp_block->size = size;
    tmp_block->free = 0;
    tmp_block->next = tmp_block1;
    struct page *np = check_page();
    tmp_block1->page = np;
    np->nb_alloc -= (sizeof (struct block) + size);
}

void *new_block(size_t size)
{
    return_page();
    struct block *tmp_block = research_block(size);
    if (tmp_block != NULL)
    {
        split_block(tmp_block, size);
    }
    else
    {
        add_page(size);
        return new_block(size);
    }
    return tmp_block->data;
}
