#ifndef MALLOC_H_
# define MALLOC_H_
void *malloc(size_t size);
void free(void* ptr);
#endif /* !MALLOC_H_*/
