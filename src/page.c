#ifndef _BSD_SOURCE
# define _BSD_SOURCE
#endif /* !_BSD_SOURCE */
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <errno.h>
#include "page.h"
#include "block1.h"

static struct page *p = NULL;

struct page *check_page()
{
    return p;
}

void use_page(struct page *np)
{
    p = np;
}

void init_page(struct page *np, size_t size)
{
    np->next = NULL;
    np->previous = NULL;
    void *v = np + 1;
    np->start = v;
    np->nb_alloc = ((size / sysconf(_SC_PAGESIZE)) + 1)
        * sysconf(_SC_PAGESIZE) - sizeof(struct page);
    np->ratio_pages = (size / sysconf(_SC_PAGESIZE)) + 1;
}
struct page *create_page(size_t size)
{
    void *p = mmap(NULL, ((size / sysconf(_SC_PAGESIZE)) + 1)
            * sysconf(_SC_PAGESIZE), PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (p == MAP_FAILED)
    {
        errno = ENOMEM;
        return NULL;
    }
    return p;
}

void return_page()
{
    struct page *np = check_page();
     while (np && np->previous != NULL)
     {
        np = np->previous;
     }
     use_page(np);
}
