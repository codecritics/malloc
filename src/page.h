#ifndef PAGE_H_
# define PAGE_H_

struct page
{
    struct page *next;
    struct page *previous;
    struct block *start;
    size_t nb_alloc;
    size_t ratio_pages;
};
struct page *check_page();
struct page *create_page(size_t size);
void init_page(struct page *np, size_t size);
void use_page(struct page *np);
void return_page();
#endif /*!PAGE_H_*/
