#ifndef BLOCK1_H_
# define BLOCK1_H_
#include "page.h"

struct block
{
    size_t size;
    struct block *next;
    int free;
    struct block *previous;
    void *data;
    struct page *page;
};
struct block *research_block(size_t size);
void *new_block(size_t size);
void init_block(struct block *s_block, size_t size);

#endif/* !BLOCK1_H_ */
